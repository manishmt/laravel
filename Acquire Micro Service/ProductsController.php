<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Response as IlluminateResponse;
use GenTux\Jwt\GetsJwtToken;
use GenTux\Jwt\JwtToken;
use App\Product;
use App\MoltinCart;
use Moltin\SDK\Facade\Moltin as Moltin;
use Config;
use Log;
use DB;
use App\ProductTransformer;
use App\FullNameParser;
use App\MoltinTransaction;
use Cocur\Slugify\Slugify;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;


class ProductsController extends Controller
{
    use GetsJwtToken;  

	protected $errors = array();
	protected $unique = array();

/*
* Authenticate moltin store credentials
* 
**/

private function moltinAuthenticate()
{
	$auth = Moltin::Authenticate('ClientCredentials', [
        'client_id'     => config('moltin.client_id'),
        'client_secret' => config('moltin.client_secret')
    ]);

    Log::info('moltin auth='.var_export($auth,1));
	return $auth;
}
    
/*
* Add product to moltin store and local DB
* 
*/
    
public function add_product(Request $request, JwtToken $jwt) {
	
	Log::info('INPUT='.var_export($request->all(),1));	

	if ($this->moltinAuthenticate()) {
		//checking required field values is exists or not
   		$validation_response = $this->validation($request->all(),false,true);
		if(empty($validation_response)){
				
			$input = $request->all();
			
			$is_enabled = 0;
			$statusLDB = 'draft';
            if (isset($input['data']['status']) && $input['data']['status'] == '1') {
                $is_enabled = 1;
                $statusLDB = 'live';
            }
        
			
			$product_data =   array(
				'title'             => $input['data']['title'], 
				'slug'              => $input['data']['slug'], 
				'sku'               => $input['data']['sku'],  
				'price'             => $input['data']['price'],
				'status'            => $is_enabled,						
				'category'			=> $input['data']['category'], 
				'stock_level'       => $input['data']['stock'], 
				'stock_status'      => 1, 					
				'requires_shipping' => 0,					
			  //'tax_band'          => $input['data']['tax_band'],
                'tax_band'          => 1424446005717238227,
				'catalog_only'      => 0,                   
				'description'       => $input['data']['description']
				);
		

			
				// Moltin create function
				$product = \Product::Create($product_data);

				Log::info('product Added='.var_export($product,1));

				if(empty($product['status']) && (!empty($product['error']) || !empty($product['errors']))){
		 		return response()->json($product);
		 		}
		 		
				if (isset($product['status']) && $product['status'] == 1) {
					$input1 = $product_data;
					unset($input1['stock_level']);
					unset($input1['requires_shipping']);
					unset($input1['stock_status']);
					unset($input1['catalog_only']);
					
					$input1['moltin_product_id'] = $product['result']['id']; 
					$input1['created_at'] = $product['result']['created_at']; 
					$input1['status'] = $statusLDB;
					$input1['stock'] = $input['data']['stock'];
					$input1['moltin_product_response'] = json_encode($product); 
					
					// Local DB entry 

					$uploaded = '';

					if(!empty($input['data']['file']['path'])) {

					$pid  = $product['result']['id'];
					$path = $input['data']['file']['path'];
					$mime = (isset($input['data']['file']['mime'])) ? $input['data']['file']['mime'] : null;
					$name = (isset($input['data']['file']['name'])) ? $input['data']['file']['name'] : null;
					$uploaded	 = $this->upload_product_image($pid,$path,$mime,$name);
					
					//Image uploaded successfully
					if(empty($uploaded)){
					DB::table('products')->insert($input1); 	
					return response()->json(['status' => 1,'code' => 200, 'messages' => 'Product successfully added','result' => $product,'image'=>'Upload failed']);
					
					}


					}
					if(!empty($uploaded['result']['url'])){
						$input1['Image'] = json_encode($uploaded['result']['url']); 
					}
					DB::table('products')->insert($input1); 	
					return response()->json(['status' => 1,'code' => 200, 'messages' => 'Product successfully added','result' => $product,'image'=>$uploaded]);
				}
			} else {
				return response()->json(['status' => 0,'messages' => 'Failed','errors' => $this->errors]);
			}
		 }else{
				return response()->json(['status' => 0,'messages' => 'Failed']);
		}
}


/*
* Upload product image on moltin
*
*/

public function upload_product_image($pid,$path,$mime = null,$name = null) {

	$file = \File::Upload($pid,$path,$mime,$name);
	
	if(isset($file['status']) && $file['status'] == true) {
		return $file;
	}else{
		return false;
	}
}


/*
* Delete produc from moltin and local DB
*
*/
public function delete_product(Request $request, JwtToken $jwt) {

		Log::info('INPUT='.var_export($request->all(),1));

		//Product id is missing
		if(empty($request->input('product_id'))){
		 return response()->json(['status' => 0,'messages' => 'Failed','errors' => "Product id is missing"]);
		}

       $product = Product::where('moltin_product_id', '=', $request->input('product_id'))->first();
	   // Product not exists
	    if(empty($product)){
		 return response()->json(['status' => 0,'messages' => 'Failed','errors' => "Product not exists"]);
	    }
	    	//Get Molting ID
		    $moltin_id = $product->moltin_product_id;
            
            if ($this->moltinAuthenticate())
            {
                $proddcutResult = \Product::Delete($moltin_id);
                	
                Log::info('moltin product deleted'.var_export($proddcutResult,1));
                
                if(empty($proddcutResult['status']) && !empty($proddcutResult['error'])){
                	//Delete if product is exists in local DB but not exists on moltin
                	if(!empty($moltin_id)) {
                	 $product->delete();			
                	}
                	
		 			return response()->json($proddcutResult);
		 		}
		 		// Delete product form local DB
                $product->delete();
		 		return response()->json($proddcutResult);
			}
    }

/*
* Update product on moltin and local DB
*
*/
   public function update_product(Request $request, JwtToken $jwt) {

		Log::info('about to update product'.var_export($request->all(),1));
	   	
        $data = array("data"=>$request->all());
        
		$validation_response = $this->validation($data,true);
			
		if(empty($validation_response)){
				
        $product = Product::where('moltin_product_id', '=', $request->input('product_id'))->first();
		 if(empty($product)){

		 	if(!empty($request->input('product_id'))){
	     	if ($this->moltinAuthenticate())
            {
	     		\Product::Delete($request->input('product_id'));	
	     	}
	     }	
		 return response()->json(['status' => 0,'messages' => 'Failed','errors' => "Product does not exists"]);
		 } 

		 
 		$allowed_fields = array('title','slug','sku','price','description','stock','category');		

        if ($this->moltinAuthenticate())
        {
            $is_enabled = 0;
            $statusLDB = 'draft';
            if ($request->input('status') == '1') {
                $is_enabled = 1;
                $statusLDB = 'live';
            } 

            $moltinProduct = \Product::Update($request->input('product_id'), [
                'title'       => $request->input('title'),
                'slug'        => $request->input('slug'),
                'sku'         => $request->input('sku'),
                'description' => $request->input('description'),
                'price'       => $request->input('price'),
                'stock_level' => $request->input('stock'),
                'category'    => $request->input('category'), 
                'status'      => $is_enabled
            ]);

            Log::info('moltin product updated'.var_export($moltinProduct,1));

           if(empty($moltinProduct['status']) && (!empty($moltinProduct['error']) || !empty($moltinProduct['errors']))){
			return response()->json(['status' => 0,'messages' => 'Failed','errors' => $moltinProduct]);
		    }

            foreach ($allowed_fields as $field) {
                if ($request->input($field) != null) {
                    $product->$field = $request->input($field);
                }
            }
		 }        
        $product->status = $statusLDB;
        $product->moltin_product_response = json_encode($moltinProduct);
        

        $response = new \stdClass;

        try {
            $product->save();
            Log::info('Product saved!!');
            $response->success = true;
            $response->message = 'Product updated.';
        }
        catch (Exception $e) {
            Log::info('OOOPS an error');
            $response->success = false;
            $response->message = 'Error updating product';
        }

        return response()->json($response);
    } else {
				return response()->json(['status' => 0,'messages' => 'Failed','errors' => $this->errors]);
	}
    }

/*
* Search product on moltin
*
*/
public function get_product_by_moltin_id(Request $request, JwtToken $jwt) {

        Log::info('input='.$request->input('product_id'));
        //Product id is missing

		if(empty($request->input('product_id'))){
		 return response()->json(['status' => 0,'messages' => 'Failed','errors' => "Product id is missing"]);
		}	

		if ($this->moltinAuthenticate())
        {
	        $productResult = \Product::Get($request->input('product_id'));
	        //If product not found on moltin
	        if(empty($productResult['status']) && !empty($productResult['error'])){
				
			$product = Product::where('moltin_product_id', '=', $request->input('product_id'))->first();
		   // Delete product exists in local DB
		    if(!empty($product)){
			 $product->delete();	
		    }
			return response()->json($productResult);
			}

			return response()->json($productResult);
    	}

    }

/*
* Get all product from local DB
*
*/
   public function get_all_products(Request $request, JwtToken $jwt){

   			$manager = new Manager();
			$manager->setSerializer(new JsonApiSerializer());        
			$products = Product::all();
			$products = new Collection($products, new ProductTransformer() , 'products');
			$products = $manager->createData($products)->toArray();
			
			return $products;


   }

/*
* Add product to cart on moltin
*/

public function add_product_to_cart(Request $request, JwtToken $jwt) {

        $cart = null;
        // get or create customer
        $merchant = $request->input('merchant');
        Log::info('INPUT='.var_export($request->all(),1));
            

        if ($this->moltinAuthenticate())
        {
            $moltinCustomer = \Customer::Find(['email' => $merchant['attributes']['email']]);
            if (count($moltinCustomer['result']) == 0) {
                $cust = $this->createMoltinCustomer($merchant);
            } else {
                $cust = $moltinCustomer['result'][0];
            }
            Log::info('moltin customer='.var_export($moltinCustomer,1));

            $validation_response = $this->validation_add_to_cart($request->all());
            if(!empty($validation_response)){
                return response()->json(['status' => 0,'messages' => 'Failed','errors' => $this->errors]);
            }

            $product = Product::find($request->input('product_id'));
            if(empty($product)){
            return response()->json(['status' => 0,'messages' => 'Failed','errors' => 'Product not exists']);
            }
            Log::info('CUST='.var_export($cust['id'],1));

            // add the child product to the cart, identified by the variation
            // for example: card_type = regular
            MoltinCart::setIdentifier($cust['id']);
            $cart = MoltinCart::Insert($product->moltin_product_id, $request->input('quantity'));
            Log::info('CART='.var_export($cart,1));
        }
        $response = new \stdClass;
        $response->cart = (isset($cart['result'])) ? $cart['result'] : $cart;
        
        return json_encode($response);
    }

/*
*   Remove product from cart moltin
*
*/

public function remove_product_from_cart(Request $request, JwtToken $jwt) {

        $cart = null;
        // get or create customer
        $merchant = $request->input('merchant');
        Log::info('INPUT='.var_export($request->all(),1));
                    
        if(empty($request->input('product_id'))) {
            return response()->json(['status' => 0,'messages' => 'Failed','errors' => 'Product Id required']);
        }

        if ($this->moltinAuthenticate())
        {
            $moltinCustomer = \Customer::Find(['email' => $merchant['attributes']['email']]);
            $cust = $moltinCustomer['result'][0];
            Log::info('moltin customer='.var_export($moltinCustomer,1));
            // add the child product to the cart, identified by the variation
            // for example: card_type = regular
            MoltinCart::setIdentifier($cust['id']);
            try {
                //$cart = MoltinCart::Update($request->input('product_id'));
                $cart = MoltinCart::Update($request->input('product_id'), [
                                       'quantity' => $request->input('quantity')
                                    ]);
                Log::info('CART='.var_export($cart,1));
            } catch (Exception $e) {
                Log::error('1ERROR='.$e->getMessage());
            }
        }

        
        $response = new \stdClass;
        $response->cart = $cart;
        return json_encode($response);
}

/*
*   Clear or empty cart
*
*/
public function clear_moltin_cart(Request $request, JwtToken $jwt) {
        $cart = null;
        // get or create customer
        $merchant = $request->input('merchant');
        Log::info('INPUT='.var_export($request->all(),1));
        if ($this->moltinAuthenticate())
        {
            $moltinCustomer = \Customer::Find(['email' => $merchant['attributes']['email']]);
            $cust = $moltinCustomer['result'][0];
            MoltinCart::setIdentifier($cust['id']);
            $cart = MoltinCart::Delete();
            Log::info('CART='.var_export($cart,1));
        }
        $response = new \stdClass;
        $response->cart = $cart;
        return json_encode($response);
    }

/*
*   Purchase cart using moltin transaction 
*
*/

public function cart_product_update(Request $request, JwtToken $jwt) {


        $cart = null;
        // get or create customer
        $merchant = $request->input('merchant');
        Log::info('INPUT='.var_export($request->all(),1));
        if ($this->moltinAuthenticate())
        {
            $validation_response = $this->validation_add_to_cart($request->all());
            
            if(!empty($validation_response)){
                return response()->json(['status' => 0,'messages' => 'Failed','errors' => $this->errors]);
            }

            $moltinCustomer = \Customer::Find(['email' => $merchant['attributes']['email']]);
           
            $cust = $moltinCustomer['result'][0];
            MoltinCart::setIdentifier($cust['id']);
            $cart = MoltinCart::Update($request->input('product_id'), [
                                        'quantity' => $request->input('quantity')
                                        ]);


            Log::info('CART='.var_export($cart,1));
        }
        $response = new \stdClass;
        $response->cart = $cart;
        return json_encode($response);

    }

/*
*	Purchase cart using moltin transaction 
*
*/
   public function purchase_products(Request $request, JwtToken $jwt) {


        // process cart order
        $order = null;
        $payment = null;
        $txn = null;

        $validation_response = $this->validation_purchase($request->all());
		if(!empty($validation_response)){
			return response()->json(['status' => 0,'messages' => 'Failed','errors' => $this->errors]);
		}	
		
        // get or create customer
        $merchant_email = $request->input('merchant_email');
        $form = $request->input('form');



        Log::info('INPUT='.var_export($request->all(),1));
        
        if ($this->moltinAuthenticate())
        {

        	$moltinCustomer = \Customer::Find(['email' => $merchant_email]);
        	$cust = $moltinCustomer['result'][0];
        	MoltinCart::setIdentifier($cust['id']);
			
			$parser = new FullNameParser();
            $name = $parser->parse_name($form['cardholder']);
            
            $cart  = MoltinCart::Contents();

            // Default shipping with 0 price | required filed 
            $shipping =  '1425907929998951000';   
            $flagShipping = false;
            foreach ($cart['result']['contents'] as $key => $content) {


                if(!empty($content['requires_shipping']['data']['key'])) {
                   if(empty($flagShipping)) {
                     //$flagShipping =   $this->get_shipping_method($content['tax_band']['data']['id']); 
                     $flagShipping =  '1424513066355130635';
                }
                }
            }
            
            $orderData = [
                    'customer' => $cust['id'],
                    'gateway' => 'dummy',
                    'bill_to' => [
                        'first_name'  => $name['fname'],
                        'last_name'   => $name['lname'],
                        'address_1'   => $form['address'],
                        'city'        => $form['city'],
                        'county'      => $form['province'],
                        'country'     => $form['country'],
                        'postcode'    => $form['postal'],
                        'phone'       => $form['phone'],
                    ],
                    'shipping' => ($flagShipping) ? $flagShipping : $shipping, // applicable shipping
                    //'shipping' => '1424513066355130635', // this ID is for free delivery
                    'ship_to' => 'bill_to'
                ];
              
            Log::info('ORDER DATA='.var_export($orderData,1));
            // Get cart content or get card

			$order = MoltinCart::Order($orderData);
            
			if(empty($order['status']) && (!empty($order['error']) || !empty($order['errors']))){
		 		return response()->json($order);
		 	}


            // make the moltin payment
            $payment = \Checkout::Payment('purchase', $order['result']['id'], [
              'data' => [ 
                'first_name'   => $name['fname'],
                'last_name'    => $name['lname'],
                'number'       => $form['cardnumber'],
                'expiry_month' => str_pad($form['exp_month'], 2, '0', STR_PAD_LEFT),
                'expiry_year'  => strval($form['exp_year']),
                'cvv'          => $form['cvv']
              ]
            ]);

            if(empty($payment['status']) && (!empty($payment['error']) || !empty($payment['errors']))){
		 		return response()->json($payment);
		 	}

            Log::info('PYMT='.var_export($payment,1));
		    
		    // save transaction record locally
            $items = [];
            foreach ($cart['result']['contents'] as $key => $content) {
                $item = [
                    'id' => $content['id'],
                    'sku' => $content['sku'],
                    'name' => $content['name'],
                    'price' => $content['price'],
                    'qty' => $content['quantity'],
                    'subtotal' => $content['totals']['post_discount']['raw']['without_tax'],
                ];
                $items[] = $item;
            }
            
            $txn = MoltinTransaction::create([
                    'reference_no' => $payment['result']['reference'],
                    'order_id' => $payment['result']['order']['id'],
                    'customer_id' => $payment['result']['order']['customer']['data']['id'],
                    'items' => $items,
                    'subtotal' => $cart['result']['totals']['post_discount']['raw']['without_tax'],
                    'tax' => $cart['result']['totals']['post_discount']['raw']['tax'],
                    'shipping' => $payment['result']['order']['shipping_price'],
                    'total' => $payment['result']['order']['total'],
                ]);

            $cart = \Cart::Delete();

        }
        $response = new \stdClass;
        $response->order = $order;
        $response->payment = $payment;
        $response->transaction = $txn;
        return json_encode($response);
    }

/**
*	Get moltin cart
*
*/
public function get_moltin_cart(Request $request, JwtToken $jwt) {


        $cart = null;
        // get or create customer
        $merchant = $request->input('merchant');
        Log::info('INPUT='.var_export($request->all(),1));
        if ($this->moltinAuthenticate())
        {
            $moltinCustomer = \Customer::Find(['email' => $merchant['attributes']['email']]);
            if (count($moltinCustomer['result']) == 0) {
                $cust = $this->createMoltinCustomer($merchant);
            } else {
                $cust = $moltinCustomer['result'][0];
            }
            MoltinCart::setIdentifier($cust['id']);
            try {
                $cart = MoltinCart::Contents();
                Log::info('CART='.var_export($cart['result'],1));
            } catch (Exception $e) {
                Log::error('ERROR='.$e->getMessage());
            }
        }
        $response = new \stdClass;
        $response->cart = $cart['result'];
        return json_encode($response);
    }


/**
*   make product featured
*   @param : product id | featured status ( yes|no ) both are required
*/
public function feature_product(Request $request, JwtToken $jwt) {

      
      $validation_response = $this->validation_featured_product($request->all());
        if(!empty($validation_response)){
            return response()->json(['status' => 0,'messages' => 'Failed','errors' => $this->errors]);
        }

      $product = Product::where('id', '=', $request->input('product_id'))->first();
      
      // Product not exists
      if(empty($product)){
        return response()->json(['status' => 0,'messages' => 'Failed','errors' => "Product not exists"]);
      } 

      if ($this->moltinAuthenticate())
        {
            if(strtolower($request->input('featured')) == 'yes') {
                $status = true;
                }else{
                $status = false;
            }

            if(!empty($status)){
             DB::table('products')->update(['featured' => false]);
            
             $moltinProduct = Product::where('moltin_product_id', $product->moltin_product_id)
                                       ->update(['featured' => $status]);
            // $moltinProduct = DB::table('products')->where('moltin_product_id', $product->moltin_product_id)
              //      ->update(['featured' => $status]);
            }else{
              $moltinProduct = Product::where('moltin_product_id', $product->moltin_product_id)
                                       ->update(['featured' => $status]);
            }
        
        Log::info('moltin product updated '.var_export($moltinProduct));
        
        $response = new \stdClass;
        $response->success = true;
        $response->message = 'Product updated successfully.';
        return json_encode($response);
        
        }else {

            return response()->json(['status' => 0,'messages' => 'Failed']);
        }
    }   

/**
*	Create customer on moltin  and returen ID
*
*/

private function createMoltinCustomer($merchant) {
        // create a customer
        $parser = new FullNameParser();
        $name = $parser->parse_name($merchant['attributes']['contact_name']);
        $moltinCustomer = MoltinCustomer::Create([
            'first_name' => $name['fname'],
            'last_name' => $name['lname'],
            'email' => $merchant['attributes']['email'],
            'password' => Hash::make($merchant['attributes']['email']), // hash the email for the password
        ]);
        Log::info('NEW moltin customer='.var_export($moltinCustomer,1));
        return $moltinCustomer['result'];
    }

/*
 * 
 * Return all available  transactions from customers specific from local DB. 
 * @param : email | optional | if not provided then it will return available transaction for current logged in user
 **/
public function get_transactions(Request $request, JwtToken $jwt) {


        $txns = null;
        // get or create customer
        $merchant = $request->input('merchant');
        $email = $request->input('email');
        
        Log::info('INPUT='.var_export($request->all(),1));
        if ($this->moltinAuthenticate())
        {

        	if(!empty($email)){
            	$moltinCustomer = \Customer::Find(['email' => $email]);
            }else{
            	$moltinCustomer = \Customer::Find(['email' => $merchant['attributes']['email']]);
            }

            Log::info('TXNS='.var_export($moltinCustomer,1));

			if (count($moltinCustomer['result']) > 0) {
                $cust = $moltinCustomer['result'][0];
                try {
                    $txns = MoltinTransaction::where('customer_id','=',$cust['id'])->orderBy('created_at','desc')->get()->toArray();
                    Log::info('TXNS='.var_export($txns,1));
                } catch (Exception $e) {
                    Log::error('ERROR='.$e->getMessage());
                }
            } 
        }
        
        $response = new \stdClass;
        $response->transactions = ($txns) ? $txns : "No transaction exists";
        return json_encode($response);

    }

/*
 * 
 * Return all available  transactions from local DB. 
 * 
 **/
public function get_all_transactions(Request $request, JwtToken $jwt) {
     	
        $txns = null;
        Log::info('INPUT='.var_export($request->all(),1));
        if ($this->moltinAuthenticate())
        {  
           //$txns = MoltinTransaction::all()->orderBy('created_at','desc')->get()->toArray();
        	$txns = MoltinTransaction::all()->toArray();
            Log::info('TXNS='.var_export($txns,1));
		}
        $response = new \stdClass;
        $response->transactions = ($txns) ? $txns : "No transaction exists";
        return json_encode($response);

    }

   /*
 * 
 * Get default tax band form moltin store
 * 
 **/
public function get_shipping_method($id) {

    $shipping = \Shipping::Listing();
    
    
        if(isset($shipping['status']) && $shipping['status'] == true) {
        
        if (!empty($shipping['result'])) {
            
            foreach ($shipping['result'] as $value) {
               
              $price = preg_replace("/[^0-9]/","",$value['price']['value']);
                if($value['tax_band']['data']['id'] == $id && $price != '000') {
                
                return $value['id'];    
              }
            }
            }else{
                return false;
            } 
        }else{
            return false;
        }
}   

 /*
 * 
 * Validate required fields
 * 
 **/
 
public function validation($data = array(),$edit = false,$add = false) {

	$this->errors = array();
	
	if(empty($data['data']['title'])) {
		$this->errors['title']   = "Required";
	}
	if(empty($data['data']['sku'])) {
		$this->errors['sku']     = "Required";
	}
	if(empty($data['data']['slug'])) {
		$this->errors['slug']     = "Required";
	}
	if(empty($data['data']['price'])) {
		$this->errors['price']    = "Required";
	}
	if(empty($data['data']['description'])) {
		$this->errors['description'] = "Required";
	}
	if(empty($data['data']['stock'])) {
		$this->errors['stock'] = "Required";
	}
	
	if(!empty($edit)){
	if(empty($data['data']['product_id'])) {
		$this->errors['product_id'] = "Required";
	}
	}
	
	if(empty($data['data']['category'])) {
		$this->errors['category'] = "Required";
	}	
	/*if(empty($data['data']['tax_band'])) {
		$this->errors['tax_band'] = "Required";
	}*/
	
	
	if(!empty($this->errors)) {
		return $this->errors;
	}else{
		return false;
		}
	
}

/*
 * 
 * Validate for purchase moltin cart
 * 
 **/
 
public function validation_purchase($data = array()) {
	
	$this->errors = array();
	
    if(empty($data['form']['cardholder'])) {
		$this->errors['cardholder']   = "Required";
	}
	
	if(empty($data['form']['exp_month'])) {
		$this->errors['exp_month']   = "Required";
	}
	
    if(empty($data['form']['exp_year'])) {
		$this->errors['exp_year']   = "Required";
	}

	if(empty($data['form']['cardnumber'])) {
		$this->errors['cardnumber']   = "Required";
	}

	if(empty($data['form']['cvv'])) {
		$this->errors['cvv']   = "Required";
	}

	if(empty($data['form']['address'])) {
		$this->errors['address']   = "Required";
	}

	if(empty($data['form']['city'])) {
		$this->errors['city']   = "Required";
	}

	if(empty($data['form']['province'])) {
		$this->errors['province']   = "Required";
	}

	if(empty($data['form']['postal'])) {
		$this->errors['postal']   = "Required";
	}

	if(empty($data['form']['phone'])) {
		$this->errors['phone']   = "Required";
	}

	if(empty($data['form']['country'])) {
		$this->errors['country']   = "Required";
	}
	
	if(empty($data['merchant_email'])) {
		$this->errors['merchant_email']   = "Required";
	}

	

	if(!empty($this->errors)) {
		return $this->errors;
	}else{
		return false;
		}


        
	
}
/**
*   Validation for add to cart product function
*
*/
public function validation_add_to_cart($data = array()) {
    $this->errors = array();
    
    if(empty($data['quantity'])) {
        $this->errors['quantity']   = "Required";
    }

    if(empty($data['product_id'])) {
        $this->errors['product_id']   = "Required";
    }

    if(!empty($this->errors)) {
        return $this->errors;
    }else{
        return false;
        }
} 
/**
*   Validation for featured product function
*
*/
public function validation_featured_product($data = array()) {
    $this->errors = array();
    $validInput = array('yes','no');

    if(empty($data['product_id'])) {
        $this->errors['product_id']   = "Required";
    }

    if(empty($data['featured'])) {
        $this->errors['featured']   = "Required";
    }

    if(!empty($data['featured']) && !in_array($data['featured'], $validInput)) {
        $this->errors['featured']   = "Allowed only yes or no";
    }

    if(!empty($this->errors)) {
        return $this->errors;
    }else{
        return false;
        }
}
    
}
