<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Response as IlluminateResponse;

use GenTux\Jwt\GetsJwtToken;
use GenTux\Jwt\JwtToken;
use Log;
use GuzzleHttp\Client;
use App\CustomClasses\Orchestra;

class ProcessorsController extends Controller
{
    use GetsJwtToken;
    
    public function get_processors(Request $request, JwtToken $jwt) {

        $processors = new Orchestra;
        $processors->method = 'GET';
        $processors->module = config('addresses.processors');
        $processors->endpoint = '/processors/get';
        $processors->data = null;
        $processors->token = $this->jwtToken();
        
        $processors = $processors->request();
        
        return response($processors, 200);
    }
    
    public function add_processor(Request $request, JwtToken $jwt) {

        $processor_data = ['processor_name' => $request->input('processor_name') , 'auth_type' => $request->input('auth_type') ];
            
        //OLD REQUEST:
        //$add_processor = orchestra_request('POST', 'addresses.processors', 'add-processor', $processor_data , $this->jwtToken());
        $add_processor = new Orchestra;
        $add_processor->method = 'POST';
        $add_processor->module = config('addresses.processors');
        $add_processor->endpoint = '/processor/add';
        $add_processor->data = $processor_data;
        $add_processor->token = $this->jwtToken();
        $add_processor = $add_processor->request();
            
        return response($add_processor, 200);
    }
    
    public function delete_processor(Request $request, JwtToken $jwt) {

        //here, we don't need the line that says
        //'auth_type' => 'required|string|max:15
        //because line ~88 only has the 'processor_name' correct?

        // Correct :) We're deleting the payment processor so we don't care what authorization type they use anymore.

        $processor_name = ['processor_name' => $request->get('processor_name') ];
            
        $delete_processor = new Orchestra;
        $delete_processor->method = 'POST';
        $delete_processor->module = config('addresses.processors');
        $delete_processor->endpoint = '/processor/delete';
        $delete_processor->data = $processor_name;
        $delete_processor->token = $this->jwtToken();
            
        $delete_processor = $delete_processor->request();
            
        return response($delete_processor, 200);
    }
    
    public function add_credit_card(Request $request, JwtToken $jwt) {

        $card_name = [
	        'name' => $request->input('name') , 
        ];
            
        $add_credit_card = new Orchestra;
        $add_credit_card->method = 'POST';
        $add_credit_card->module = config('addresses.merchants');
        $add_credit_card->endpoint = '/credit-card/add';
        $add_credit_card->data = $card_name;
        $add_credit_card->token = $this->jwtToken();
            
        $add_credit_card = $add_credit_card->request();
            
        return response($add_credit_card, 200);
    }

    public function get_credit_cards(Request $request, JwtToken $jwt) {

        $cards = new Orchestra;
        $cards->method = 'GET';
        $cards->module = config('addresses.merchants');
        $cards->endpoint = '/credit-cards/get';
        $cards->data = null;
        $cards->token = $this->jwtToken();

        $cards = $cards->request();

        return response($cards, 200);
    }
}
