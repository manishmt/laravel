<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Input;
use Redirect;
use Response;
use DB;
use App\Testmethod;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;

class TestmethodsController extends Controller
{

    protected $rules = [
        'title' => ['required', 'min:3'],
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        
        $filters = array();
        if ($request->session()->has('search')) {
            $search = $request->session()->get('search');
            $filters['search'] = $search;
            
        }
        if ($request->session()->has('published')) {
            $published = $request->session()->get('published');
            $filters['published'] = $published;
            
        }
        if ($request->session()->has('categories')) {
            $categories1 = $request->session()->get('categories');
            $filters['categories'] = $categories1;
            
        }
        
        $categories = DB::table('categories')->orderBy("title",'asc')->lists('title','id');
        $query = DB::table('testmethods');
        $query->leftJoin('categories', 'categories.id', '=', 'testmethods.cat_id');
        $query->select('testmethods.*', 'categories.title as category');
        if(isset($published)){
        $query->where("testmethods.published",$published);
        }
        if(isset($search)){
        $query->where("testmethods.title",'like','%'.$search.'%');
        $query->orWhere("testmethods.id",'like','%'.$search.'%');
        $query->orWhere("categories.title",'like','%'.$search.'%');
        $query->orWhere("testmethods.barcolor",'like','%'.$search.'%');
        }
        if(isset($categories1)){
        $query->where("categories.id",$categories1);
        }
        $query->orderBy("id",'desc');
        $testmethods = $query->get();
        
        $table = 'testmethods';
        return view('testmethods.index', compact('testmethods','categories','filters','table'));
    }
    
   

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {


        $categories = DB::table('categories')->orderBy('title', 'asc')->lists('title','id');

        return view('testmethods.create', array('categories' => $categories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

    $this->validate($request, $this->rules);

    $input = Input::all();
    Testmethod::create( $input );
 
    return Redirect::route('testmethods.index')->with('message', '<strong>Success!</strong> Testmethod have been created successfully.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
       $testmethod = Testmethod::find($id);
       $categories = DB::table('categories')->orderBy('title', 'asc')->lists('title','id');

    return view('testmethods.edit', compact('testmethod', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    $validator = Validator::make($request->all(), [
            'title' => 'required|max:255'
            
        ]);


        if ($validator->fails()) {
            return Redirect::to('testmethods/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
            $testmethod = Testmethod::find($id);
            $testmethod->title            = Input::get('title');
            $testmethod->alias            = Input::get('alias');
            $testmethod->barcolor            = Input::get('barcolor');
            $testmethod->description      = Input::get('description');
            $testmethod->published        = Input::get('published');
            $testmethod->cat_id               = Input::get('cat_id');
            $testmethod->link               = Input::get('link');
            $testmethod->save();

            // redirect
        return Redirect::route('testmethods.index')->with('message', '<strong>Success!</strong> Testmethod have been updated successfully.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {


        $testmethod = Testmethod::find($id);
        $testmethod->delete();
        
        return Redirect::route('testmethods.index',Input::all())->with('message', '<strong>Success!</strong> Testmethod have been deleted successfully.');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show(Request $request)
    {

        $params = $request->all();
        
        $query = DB::table('testmethods');
        
            if(isset($params['id'])){
            $query->where("id",$params['id']);
            }
            if(isset($params['title'])){
            $query->where("title",$params['title']);
            }
            if(isset($params['published'])){
            $query->where("published",$params['published']);
            }
            if(isset($params['cat_id'])){
            $query->where("cat_id",$params['cat_id']);
            }
            if(isset($params['barcolor'])){
            $query->where("barcolor",$params['barcolor']);
            }
            $testmethods = $query->get();
        

        return Response::json(array(
            'error' => false,
            'testmethods' => $testmethods,
            'status_code' => 200
            ));
        
    }
    
    /**
     * Updating status form listing view 
     * 
     */
    public function status($id){
            $testmethod = Testmethod::find($id);
            $testmethod->published        = !empty($testmethod->published) ? '0' : '1';
            $testmethod->save();
            return Redirect::route('testmethods.index')->with('message', '<strong>Success!</strong> Testmethod have been deleted successfully.');
    }
    
    /**
     * Filter Data 
     * 
     */    
    
      public function filterData(Request $request){
            if(Input::get('published') != "#"){
                session(['published' => Input::get('published')]);    
            }else{
                    $request->session()->forget('published');
            }
            if(Input::get('search')){
             session(['search' => Input::get('search')]);    
            }
            if(Input::get('categories') != "#"){
             session(['categories' => Input::get('categories')]);    
            }else{
                    $request->session()->forget('categories');
            }
            
                
            return Redirect::route('testmethods.index');
    } 
    
    /**
     * Session Reset
     * 
     */     
     public function sesstionReset(Request $request){
            
            $request->session()->forget('published');
            $request->session()->forget('search');
            $request->session()->forget('categories');
            $request->session()->forget('manufacturers');
            $request->session()->forget('headers');
        
            return Redirect::route('testmethods.index');
    }
}
