<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Input;
use Redirect;
use Response;

use DB;
use Illuminate\Routing\Route;
use App\RankingQualities;
use Illuminate\Support\Facades\Validator;

class RankingQualitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    
    protected $rules = [
        'title' => ['required', 'min:3'],
    ];
    
    protected $types = array(
                            "SIMPLE" => "Simple",
                            "OVERALL" => "Overall"
                            );

public function index(Request $request)
    {
        
        
        if ($request->session()->has('currentPage')) {
            //$this->sesstionReset();
        }    
        $filters = array();
        if ($request->session()->has('search')) {
            $search = $request->session()->get('search');
            $filters['search'] = $search;
            
        }
        if ($request->session()->has('published')) {
            $published = $request->session()->get('published');
            $filters['published'] = $published;
            
        }
        if ($request->session()->has('categories')) {
            $categories1 = $request->session()->get('categories');
            $filters['categories'] = $categories1;
            
        }
        if ($request->session()->has('testmethods')) {
            $testmethods1 = $request->session()->get('testmethods');
            $filters['testmethods'] = $testmethods1;
            
        }
        
        $categories = DB::table('categories')->orderBy("title",'asc')->lists('title','id');
        //$testmethods = DB::table('testmethods')->orderBy('id', 'desc')->groupBy('barcolor')->lists('title','id');
        $testmethods = array();
        $testMethodsQuery = DB::table('testmethods as t');        
        if(isset($categories1)){
         $testMethodsQuery->where("t.cat_id",$categories1);
         $testmethods = $testMethodsQuery->orderBy("title",'asc')->lists('title','id');                            
        }
        $query = DB::table('ranking_qualities');
        $query->leftJoin('categories', 'categories.id', '=', 'ranking_qualities.cat_id');
        $query->select('ranking_qualities.*', 'categories.title as category');
        if(isset($published)){
            $query->where("ranking_qualities.published",$published);
        }    
        if(isset($search)){
        $query->where("ranking_qualities.title",'like','%'.$search.'%');
        $query->orWhere("ranking_qualities.id",'like','%'.$search.'%');
        $query->orWhere("ranking_qualities.type",'like','%'.$search.'%');
        $query->orWhere("categories.title",'like','%'.$search.'%');
        }
        if(isset($categories1)){
        $query->where("categories.id",$categories1);
        }
        if(isset($testmethods1)){
        
        $query->where("ranking_qualities.testmethod_id",$testmethods1);
        }
        $query->orderBy("ordering",'asc');
        $ranking_qualities = $query->get();
        $table = 'ranking_qualities';
        return view('ranking-qualities.index', compact('ranking_qualities','categories','filters','table','testmethods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $testmethods = array();
        $catId = '';
        $categories = DB::table('categories')->orderBy('title', 'asc')->lists('title','id');
        if( $request->session()->has('categories') && !empty($request->session()->get('categories'))){
                    $catId = $request->session()->get('categories');
        
        $testmethods = DB::table('testmethods')->where('cat_id', '=', $catId)->lists('title','id');
        }            
        //$testmethods = DB::table('testmethods')->orderBy('id', 'desc')->groupBy('barcolor')->lists('title','id');
        return view('ranking-qualities.create', array('categories' => $categories,'types' => $this->types,'testmethods' => $testmethods,'idCreate' => $catId,));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

    
    $input = Input::all();
    if(isset($input['select_to_copy']) && isset($input['ids_to_copy'])){
            $ids = explode("~", $input['ids_to_copy']);
            $ids = array_filter($ids);
            foreach ($ids as $id) {
            $ranking_qualities = RankingQualities::find($id);
            $dataArr = array();    
            
            $dataArr['cat_id']       = $ranking_qualities->cat_id;
            $dataArr['title']        = $ranking_qualities->title;      
            $dataArr['description']  = $ranking_qualities->description;
            $dataArr['type']           = $ranking_qualities->type;
            $dataArr['unit']         = $ranking_qualities->unit;
            $dataArr['sql']          = $ranking_qualities->sql;
            $dataArr['limit']        = $ranking_qualities->limit;
            $dataArr['tolerance']        = $ranking_qualities->tolerance;
            $dataArr['children']     = $ranking_qualities->children;
            $dataArr['benchmark']    = $ranking_qualities->benchmark;
            $dataArr['numberformat'] = $ranking_qualities->numberformat;
            $dataArr['weight']       = $ranking_qualities->weight;
            $dataArr['published']    = $ranking_qualities->published;
            $dataArr['considered']    = $ranking_qualities->considered;
            $dataArr['ordering']    = $ranking_qualities->ordering;
            $dataArr['testmethod_id'] = $input['select_to_copy'];
            
            RankingQualities::create( $dataArr );
            }
            //RankingQualities::create( $input );    
            return Redirect::route('ranking-qualities.index')->with('message', '<strong>Success!</strong> Ranking Quality have been created successfully.');    
            exit;            
    }
    
    
    $this->validate($request, $this->rules);
    
    RankingQualities::create( $input );
 
    return Redirect::route('ranking-qualities.index')->with('message', '<strong>Success!</strong> Ranking Quality have been created successfully.');


        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
       $ranking_qualities = RankingQualities::find($id);
       
       $categories = DB::table('categories')->orderBy('title', 'asc')->lists('title','id');
       $types = $this->types;
       
       $testmethods = DB::table('testmethods')->where('cat_id', '=', $ranking_qualities['cat_id'])->lists('title','id');
       //$testmethods = DB::table('testmethods')->orderBy('id', 'desc')->groupBy('barcolor')->lists('title','id');
       
       return view('ranking-qualities.edit', compact('ranking_qualities', 'categories','types','testmethods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    $validator = Validator::make($request->all(), [
            'title' => 'required|max:255'
            
        ]);


        if ($validator->fails()) {
            return Redirect::to('ranking-qualities/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
            $ranking_qualities = RankingQualities::find($id);
            $ranking_qualities->title            = Input::get('title');
            $ranking_qualities->description      = Input::get('description');
            $ranking_qualities->published        = Input::get('published');
            $ranking_qualities->cat_id           = Input::get('cat_id');
            $ranking_qualities->numberformat     = Input::get('numberformat');
            $ranking_qualities->unit             = Input::get('unit');
            $ranking_qualities->weight             = Input::get('weight');
            $ranking_qualities->tolerance         = Input::get('tolerance');
            $ranking_qualities->benchmark         = Input::get('benchmark');
            $ranking_qualities->children         = Input::get('children');
            $ranking_qualities->sql                 = Input::get('sql');
            $ranking_qualities->limit             = Input::get('limit');
            $ranking_qualities->type             = Input::get('type');
            $ranking_qualities->testmethod_id     = Input::get('testmethod_id');
            
            $ranking_qualities->save();

            // redirect
        return Redirect::route('ranking-qualities.index')->with('message', '<strong>Success!</strong> Ranking Quality have been updated successfully.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

      $ranking_qualities = RankingQualities::find($id);
    $ranking_qualities->delete();

    return Redirect::route('ranking-qualities.index',Input::all())->with('message', '<strong>Success!</strong> Ranking Quality have been deleted successfully.');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show(Request $request,$id = false)
    {

        $params = $request->all();
        
            $query = DB::table('ranking_qualities');
            if(isset($params['id'])){
            $query->where("id",$params['id']);
            }
            if(isset($params['title'])){
            $query->where("title",$params['title']);
            }
            if(isset($params['published'])){
            $query->where("published",$params['published']);
            }
            if(isset($params['cat_id'])){
            $query->where("cat_id",$params['cat_id']);
            }
            if(isset($params['cat_id'])){
            $query->where("cat_id",$params['cat_id']);
            }
            $ranking_qualities = $query->get();
        

        return Response::json(array(
            'error' => false,
            'ranking_qualities' => $ranking_qualities,
            'status_code' => 200
            ));
        
    }
    /**
     * Updating status form listing view 
     * 
     */
    public function status($id){
            $ranking_qualities = RankingQualities::find($id);
            $ranking_qualities->published        = !empty($ranking_qualities->published) ? '0' : '1';
            $ranking_qualities->save();
            return Redirect::route('ranking-qualities.index')->with('message', '<strong>Success!</strong> Ranking Quality have been deleted successfully.');
    }
    
      public function filterData(Request $request){
            if(Input::get('published') != "#"){
                session(['published' => Input::get('published')]);    
            }else{
                    $request->session()->forget('published');
            }
            if(Input::get('search')){
             session(['search' => Input::get('search')]);    
            }
            if(Input::get('categories') != "#"){
             session(['categories' => Input::get('categories')]);    
            }else{
                    $request->session()->forget('categories');
            }
            if(Input::get('testmethods') != "#"){
             session(['testmethods' => Input::get('testmethods')]);    
            }else{
                    $request->session()->forget('testmethods');
            }
                
            return Redirect::route('ranking-qualities.index');
    } 
     public function sesstionReset(Request $request){
            
            $request->session()->forget('published');
            $request->session()->forget('search');
            $request->session()->forget('categories');
            $request->session()->forget('testmethods');
            $request->session()->forget('manufacturers');
            $request->session()->forget('headers');
        
            return Redirect::route('ranking-qualities.index');
    }


    public function regenerateRanking(){

        $this->clearAllRankingData();
        $rankings = $this->getAllRankingData();
        
        foreach ($rankings as $id=>$ranking){
                if ($ranking->type =='SIMPLE'){

                    $this->rankingInsertSimple($ranking);  
                    }
                
           }

          $published=array();
          foreach ($rankings as $id=>$ranking)
            {
            
            if ($ranking->type =='OVERALL'){
                    
                    $this->rankingInsertAggregate($ranking,implode(',',$published));                  
               } 
            if ($ranking->published) {$published[]=$id; }
         }
         
         echo "Regenerate Ranking process completed...";
     }

    public function clearAllRankingData(){
        DB::delete('DELETE r.* FROM ranking AS r LEFT JOIN ranking_qualities AS q ON q.id=r.quality_id');
        
    }
    public function getAllRankingData(){

         $query = DB::table('ranking_qualities as q');
            //$query->where('q.cat_id', '133');
            //$query->where('q.testmethod_id', '17');
            $query->where('q.testmethod_id','!=', '');
            $query->orderBy('q.ordering', 'desc');
              $rankings =    $query->get();
              
              $rankingsAll = array();
              foreach ($rankings as $value) {
                      $rankingsAll[$value->id] = $value;
              }
              return  $rankingsAll;

    }

    public function rankingInsertSimple($ranking = array()){

    $benchmark=$ranking->benchmark;
       $aId = $this->getRankingClassAttributeID($ranking->cat_id);  
        

       $query = DB::table('benchmarks as b');
                $query->leftJoin('category_benchmarks as cb', 'cb.bench_id', '=', 'b.id');
                $query->select(db::raw('b.title as name,b.parameter_max,b.parameter_min,b.parameter_step'));
                    $query->where("cb.bench_id",$benchmark);    
                    if(isset($ranking->cat_id)){
                    $query->where("cb.cat_id",$ranking->cat_id);    
                    }
                    $benchmarksById = $query->get();
                    
                        $see = ',AVG(t.data) as data';
                        if(empty($ranking->sql)){
                        if(isset($benchmarksById[0]->parameter_max) && is_numeric($benchmarksById[0]->parameter_max)){
                                $parameter_max = $benchmarksById[0]->parameter_max;
                                $parameter_min = $benchmarksById[0]->parameter_min;
                                 if(!empty($benchmarksById[0]->parameter_step)){
                                    $divisior = 1+($parameter_max - $parameter_min)/$benchmarksById[0]->parameter_step;
                                    //$see = ",AVG(t.data) as data";    
                                    $see = ",SUM(t.data)/$divisior as data";    



                            }
                            }else{
                                if(isset($benchmarksById[0]->parameter_max)){
                             $parameter_max = ord(strtolower($benchmarksById[0]->parameter_max))-96;
                             $parameter_min = ord(strtolower($benchmarksById[0]->parameter_min))-96;
                             if(!empty($benchmarksById[0]->parameter_step)){
                                $divisior = 1+($parameter_max - $parameter_min)/$benchmarksById[0]->parameter_step;
                                $see = ",SUM(t.data)/$divisior as data";    




                            }
                            }
                        }    
                        }
$sql="SELECT p.id AS productid $see, v.data AS class, tt.barcolor,t.testmethod
       FROM products AS p
       LEFT JOIN product_testmethod as p2t ON p2t.product_id=p.id
       LEFT JOIN testmethods as tt ON tt.id=p2t.testmethod_id
       LEFT JOIN product_attribute_values AS v ON v.attribute_id=$aId AND v.product_id=p.id
       LEFT JOIN benchmark_test_values AS t ON t.benchid IN ($benchmark) {$ranking->sql} AND t.productid=p.id 
       WHERE p.published>0 AND p.archive=0 AND p.cat_id={$ranking->cat_id}
       AND (t.data IS NOT NULL) and t.testmethod = {$ranking->testmethod_id} and tt.id = {$ranking->testmethod_id}
       GROUP BY p.id ORDER BY data DESC, class, barcolor ASC";


    
    $answers = DB::select( $sql );
    /* Insert Ranking data */
    $this->rankingInsertInDatabase($ranking,$answers,$benchmark,$aId);
}

function rankingInsertAggregate($ranking,$qualities=null)
{
    
            
    $mode=false;
    if ($mode) //echo 'ALLranking'.$ranking->cat_id;

   if (($ranking->children)or(!$qualities)) $qualities=$ranking->children;
   
   $selectdata = 'AVG(r.'.(($mode)?'rank':'classrank').'*q.weight)';//:'AVG(r.data)';
   $order = 'ASC';
   $selectrank = $selectdata;
    
 


    $sql="SELECT p.id AS productid, $selectrank AS volgorde, $selectdata AS data, r.class AS class, tt.barcolor,r.testmethod
   FROM products AS p
   LEFT JOIN ranking AS r ON r.quality_id IN ($ranking->children) AND r.product_id=p.id
   LEFT JOIN ranking_qualities AS q ON q.id=r.quality_id
   LEFT JOIN testmethods as tt ON tt.id=r.testmethod
   WHERE p.published>0 AND p.archive=0 AND p.cat_id={$ranking->cat_id}
   AND (r.rank IS NOT NULL) and r.testmethod = {$ranking->testmethod_id} and tt.id = {$ranking->testmethod_id}
   GROUP BY p.id ORDER BY volgorde $order, class ASC";
          
    
   $answers = DB::select( $sql );
   $this->rankingInsertInDatabaseAggregate($ranking,$answers);
}

function rankingInsertInDatabaseAggregate($ranking,$answers)
{   

               
   $class=array();
   $classdata=array();
   $rank=0;
   $data=1e15;
   $productTestmethodsAll =array();
   foreach ($answers as $a)
    {

        

     $c=$a->class.$a->testmethod;
     //capping
     if (($ranking->limit)&&($a->data>$ranking->limit))
             { $a->data=$ranking->limit; }
    //with tolerance             
    if ( (($ranking->tolerance)&&($a->data<($data*(1-$ranking->tolerance/100))))
            or
            ((!$ranking->tolerance)&&($a->data!=$data)) ) {
          $data=$a->data;
          $rank++; 
          }          
    if (!isset($class[$c])) { $class[$c]=1; $classdata[$c]=$data; }
       else if ($classdata[$c]!=$data) 
             {
              $class[$c]++;
              $classdata[$c]=$data;
             }
    $rankclass=$class[$c];
          $productTestmethodsAll[] = array(
                                'quality_id' => $ranking->id,
                                'rank' => $rank,
                                'class' => $a->class,
                                'classrank' => $rankclass,
                                'product_id' => $a->productid,
                                'data' => $a->data,
                                'testmethod'=>$a->testmethod
                        );
        
    

        }

         DB::table('ranking')->insert($productTestmethodsAll); 

}

/* Insert Ranking data */
function rankingInsertInDatabase($ranking,$answers,$benchmark,$aId)
{   

    
   $class=array();
   $classdata=array();
   $rank=0;
   $data=1e15;
   $i = 0;
   $productTestmethodsAll = array();

    $newarray =array();

   foreach ($answers as $a)
   {



    $c=$a->class.$a->testmethod;
     //capping
     if (($ranking->limit)&&($a->data>$ranking->limit))
             { $a->data=$ranking->limit; 


}
    //with tolerance             
    if ( (($ranking->tolerance)&&($a->data<($data*(1-$ranking->tolerance/100))))
            or
            ((!$ranking->tolerance)&&($a->data!=$data)) ) {


          $data=$a->data;
          $rank++; 



          }          
    if (!isset($class[$c])) { $class[$c]=1; $classdata[$c]=$data; }
       else if ($classdata[$c]!=$data) 
             {
              $class[$c]++;
              $classdata[$c]=$data;
             }
           $rankclass=$class[$c];
          /*Inserting new  ranking data */
        
  
      $productTestmethodsAll[] = array(
                                'quality_id' => $ranking->id,
                                'rank' => $rank,
                                'class' => $a->class,
                                'classrank' => $rankclass,
                                'product_id' => $a->productid,
                                'data' => $a->data,
                                'testmethod'=>$a->testmethod
                        );
        

        
    
  }    
         /* ranking value inserting */
         DB::table('ranking')->insert($productTestmethodsAll);   

    
}
    public function getRankingClassAttributeID($cat_id){
    
        $query = DB::table('categories');
        $query->select('class_attributeid');
            $query->where('id', $cat_id);
              $class_attributeid =    $query->get();
        
          
 if (!$class_attributeid[0]->class_attributeid) { echo 'No class attributeid for this category. Extremely fatal error.'; exit(); }
            return $class_attributeid[0]->class_attributeid;
            //old
        if ($cat_id==138) return 219;
        return 176;
    }
}
